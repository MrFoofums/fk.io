import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { AnimationService } from '../service/animation.service';
import { HTML5CanvasObject } from '../interface/html5-canvas-object';
import { AnimationComponent } from '../interface/animation-component';
import { UtilsService } from '../service/utils.service';

@Component({
  selector: 'app-prisms',
  templateUrl: './prisms.component.html',
  styleUrls: ['./prisms.component.css']
})
export class PrismsComponent implements  AnimationComponent, AfterViewInit {
  @ViewChild('drawHere') canvas;
  @ViewChild('container') container;

  prisms: Array<Prism>;
  animation: AnimationService;
  
  constructor(private gen: UtilsService) { 
    this.prisms = [];
  }

  init() {
    for (let i = 0; i < 100; i ++) {
      // tslint:disable-next-line:max-line-length
      this.prisms.push(this.randomPrism());
    }
  }

  randomPrism(): Prism {
    const x = this.gen.randomIntFromRange(0, this.animation.canvasWidth);
    const y = this.gen.randomIntFromRange(-50, -500);
    const height = this.gen.randomIntFromRange(4, 200);
    const width = Math.floor(height / 5);
    const dy = this.gen.randomIntFromRange(2, 8);
    const color = this.gen.randomColor(this.gen.darkColors);
  
    return new Prism(x, y, width, height, dy, this.animation.canvasWidth, this.animation.canvasHeight, color);
  }

  ngAfterViewInit() {
    this.animation = new AnimationService(this.container, this.canvas);
    this.animation.setUpCanvasAndContext();
    this.init();
    this.animation.animate(this.prisms);
  }

}


class Prism implements HTML5CanvasObject {
  x: number;
  y: number;

  dy: number;

  height: number;
  width: number;

  color: string;

  gravity: 1;
  gen: UtilsService;

  canvasHeight: number;
  canvasWidth: number;
  constructor(x: number, y: number, width: number, height: number, dy: number, canvasWidth: number, canvasHeight: number, color: string) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.color = color;

    this.dy = dy;
    this.gen = new UtilsService();

    this.canvasHeight = canvasHeight;
    this.canvasWidth = canvasWidth;
  }


  draw(c:CanvasRenderingContext2D){
      c.beginPath();
      c.fillRect(this.x,this.y,this.width,this.height);
      c.fillStyle = this.color;
      c.fill();
      c.closePath();
    }

   update(c: CanvasRenderingContext2D) {
    if (this.y - this.height > this.canvasHeight) {
      this.x = this.gen.randomIntFromRange(0, this.canvasWidth);
      this.y = this.gen.randomIntFromRange(0 - this.height, -500);
      this.dy = this.gen.randomIntFromRange(2, 10);
    }

    this.draw(c);
    this.y += this.dy;
   }
}

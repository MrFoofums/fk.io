import { Component, ViewChildren, ElementRef, Input } from '@angular/core';
import { Particle } from '../shared/model/particle';
import { AnimationComponent } from './interface/animation-component';
import { AnimationService } from './service/animation.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor() {
  }

}

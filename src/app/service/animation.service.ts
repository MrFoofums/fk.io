import { Injectable } from '@angular/core';
import { HTML5CanvasObject } from '../interface/html5-canvas-object';
import { Scene, PerspectiveCamera, WebGLRenderer, Mesh, ParticleSystem } from 'three';
import * as THREE from 'three';
import { ThreeDimensionalObject } from '../interface/three-dimensional-object';

export class AnimationService {
  canvas: any;
  context: CanvasRenderingContext2D;

  canvasWidth: number;
  canvasHeight: number;
  container: HTMLElement;

  scene: Scene;
  camera: PerspectiveCamera;
  renderer: WebGLRenderer;


  constructor(container: any, canvas: any) {
    this.container = container.nativeElement;
    this.canvas = canvas.nativeElement;

  }

  setUpCanvasAndContext() {
    this.canvasHeight = this.container.offsetHeight;
    this.canvasWidth = this.container.offsetWidth;

    this.canvas.setAttribute('width', this.canvasWidth);
    this.canvas.setAttribute('height', this.canvasHeight);
    this.context = this.canvas.getContext('2d');

    // console.log(this.canvas);
  }

  setUp3DCanvasAndContext() {
    this.canvasHeight = this.container.offsetHeight;
    this.canvasWidth = this.container.offsetWidth;

    this.canvas.setAttribute('width', this.canvasWidth);
    this.canvas.setAttribute('height', this.canvasHeight);

    this.scene = new THREE.Scene();
    this.camera = new THREE.PerspectiveCamera( 55, this.canvasWidth / this.canvasHeight , 2, 2000 );

    this.backtoZero();

    this.renderer = new THREE.WebGLRenderer();
    this.renderer.setSize( this.canvasWidth, this.canvasHeight );
    this.container.appendChild(this.renderer.domElement);
    // document.body.appendChild( this.renderer.domElement ); // TODO
  }

  backtoZero() {
    this.canvas.setAttribute('width', 0);
    this.canvas.setAttribute('height', 0);
  }

  animate(objects: Array<HTML5CanvasObject>) {
    window.requestAnimationFrame(() => this.animate(objects));
    this.context.clearRect(0, 0, this.canvasWidth, this.canvasHeight);

    // Updaet everything
    this.updateObjects(objects);

    // Put my name in The middle for no good Reason
    this.context.font = '30px Open Sans';
    this.context.textAlign = 'center';
    this.context.fillText('Forrest Knight', this.canvasWidth / 2, this.canvasHeight / 2);
  }

  animate3D( object: Mesh) {
    window.requestAnimationFrame(() => this.animate3D(object));
    object.rotation.x += 0.01;
    object.rotation.y += 0.01;
    this.renderer.render( this.scene, this.camera );
  }

  animate3DObjects( objects: Array<ThreeDimensionalObject>) {
    window.requestAnimationFrame(() => this.animate3DObjects(objects));
    objects.forEach(o => {
      o.update();
    });
    this.renderer.render( this.scene, this.camera );
  }

  runParticleSystem(system: ParticleSystem): any {
    window.requestAnimationFrame(() => this.runParticleSystem(system));
    system.rotation.y += 0.01;
    this.renderer.render( this.scene, this.camera );
  }

  updateObjects(objects: Array<HTML5CanvasObject>) {
    objects.forEach(obj => {
      obj.update(this.context);
    });
  }

  getMaximumRadiusForCenteredSphere(): number {
    if (this.canvasWidth < this.canvasHeight) {
      return this.canvasWidth / 2;
    } else {
      return this.canvasHeight / 2;
    }
  }
}

import { Injectable } from '@angular/core';
import { Scene, PerspectiveCamera, WebGLRenderer, ParticleSystem, Geometry, PointsMaterial, Points } from 'three';
import * as THREE from 'three';

export class ParticleService {
  canvas: any;
  context: CanvasRenderingContext2D;

  canvasWidth: number;
  canvasHeight: number;
  container: HTMLElement;

  scene: Scene;
  camera: PerspectiveCamera;
  renderer: WebGLRenderer;

  geometry: Geometry;
  material: PointsMaterial;

  points: Points;

  constructor(container: any, canvas: any) {
    this.container = container.nativeElement;
    this.canvas = canvas.nativeElement;
    this.geometry = new Geometry();
  }

  setUp(){
    this.setUpCanvas()
    this.setUpCamereInitScene();
  }

  setUpCanvas() {
    this.canvasHeight = this.container.offsetHeight;
    this.canvasWidth = this.container.offsetWidth;

    this.canvas.setAttribute('width', this.canvasWidth);
    this.canvas.setAttribute('height', this.canvasHeight);
  }

  setUpCamereInitScene(){
    this.camera = new PerspectiveCamera(55, this.canvasWidth/this.canvasHeight , 2, 2000)
    this.backtoZero();
    this.camera.position.z = 1000;
    
    this.scene = new Scene();
  }

  initPoints(){
    this.material = new PointsMaterial();
    this.points = new Points(this.geometry, this.material);
  }

  addRendereToScene(){
    this.renderer = new WebGLRenderer();
    this.renderer.setSize( this.canvasWidth, this.canvasHeight );
    this.container.appendChild(this.renderer.domElement);
    
    this.scene.add(this.points);
  }


  backtoZero() {
    this.canvas.setAttribute('width', 0);
    this.canvas.setAttribute('height', 0);
  }
 

}

import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { AnimationComponent } from '../interface/animation-component';
import { AnimationService } from '../service/animation.service';
import { UtilsService } from '../service/utils.service';
import { HTML5CanvasObject } from '../interface/html5-canvas-object';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-rain',
  templateUrl: './rain.component.html',
  styleUrls: ['./rain.component.css']
})
export class RainComponent implements AnimationComponent, AfterViewInit {
  @ViewChild('drawHere') canvas;
  @ViewChild('container') container;

  rainDropsContainer: RainDropsContainer;
    constructor() {
  }

  init() {}

  ngAfterViewInit() {
    this.rainDropsContainer = new RainDropsContainer(this.canvas, this.container);
  }
}

class RainDropsContainer {
  raindrops: Array<RainDrop>;
  animation: AnimationService;
  canvas: any;
  container: any;
  utils: UtilsService;

  rainRadius = 20;
  lastId: number = 0;

  constructor(canvas: any, container: any) {
    this.raindrops = [];
    this.utils = new UtilsService();
    this.canvas = canvas;
    this.container = container;
    this.animation = new AnimationService(this.container, this.canvas);


    this.start();
  }

  start() {
    this.animation.setUpCanvasAndContext();
    this.init();
    this.animation.animate(this.raindrops);
  }

  init() {
    for (let i = 0; i < 3; i++) {
      this.raindrops.push(this.rainMaker());
    }

    this.raindrops.forEach(orb => {
      orb.setBounds(this.animation.canvasWidth, this.animation.canvasHeight);
    });
  }

  splatter(oldDrop: RainDrop) {
    const splatterFactor = this.utils.randomIntFromRange(3, 14);
    for( var i = 0; i < splatterFactor; i++){
      // console.log('exploding');
      const vel = {
        x: this.utils.randomIntFromRange(-1, 1),
        y: this.utils.randomIntFromRange(-1, -3)
      };
      this.lastId+=1;
      const newDrop = new RainDrop(this.lastId, oldDrop.x , oldDrop.y, vel, 2, this.utils.randomColor(this.utils.darkColors), 1, this);
      newDrop.setBounds(this.canvas.width, this.canvas.height);
      newDrop.isSplatter = true;
      this.raindrops.push(newDrop);
      // Need to destroy splatter
      console.log(this.raindrops);
    }
  }
  
  resetOldDrop(oldDrop: RainDrop) {
    // Reset the old one
    oldDrop.x = this.utils.randomIntFromRange(this.rainRadius, this.animation.canvasWidth - this.rainRadius);
    oldDrop.y = this.utils.randomIntFromRange(-1500, 0);
    oldDrop.velocity.y = 0;

  }

  rainMaker(): RainDrop {
    const x = this.utils.randomIntFromRange(this.rainRadius, this.animation.canvasWidth - this.rainRadius);
    const y = this.utils.randomIntFromRange(-1500, 0);
    const vel = {
      x: 0,
      y: 0
    };
    this.lastId+=1;
    return new RainDrop(this.lastId, x, y, vel, this.rainRadius, this.utils.randomColor(this.utils.darkColors), 5, this);
  }
}

class RainDrop implements HTML5CanvasObject {
  id:number;
  x: number;
  y: number;
  gravity: number;

  ttl: number;

  radius: number;
  color: string;

  velocity: {
    x: number;
    y: number;
  };

  canvasWidth: number;
  canvasHeight: number;
  rainDropsContainer: RainDropsContainer;

  isSplatter: boolean = false;

  constructor(id:number, x: number, y: number, velocity: any, radius: number, color: string, ttl: number, rainDropsContainer: RainDropsContainer) {
    this.x = x;
    this.y = y;
    this.radius = radius;
    this.color = color;
    this.velocity = velocity;
    this.gravity = 0.08;

    this.ttl = ttl;
    this.rainDropsContainer = rainDropsContainer;
  }

  draw(c: CanvasRenderingContext2D): void {
        c.beginPath();
        c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
        c.fillStyle = this.color;
        c.fill();
        c.closePath();
  }

  update(c: CanvasRenderingContext2D): void {
      this.draw(c);

    if (this.x - this.radius <= 0 || this.x + this.radius >= this.canvasWidth) {
      this.velocity.x = -this.velocity.x;
     }
    if ( this.y + this.radius >= this.canvasHeight && !this.isSplatter) {
      //  this.collisionFunc();
        this.rainDropsContainer.splatter(this);
      this.rainDropsContainer.resetOldDrop(this);
     }
    //  else if(this.y + this.radius >= this.canvasHeight && this.isSplatter) {
    //    this.rainDropsContainer.raindrops = this.rainDropsContainer.raindrops.filter(function(val,index,arr){
    //       return index != this.id-1;
    //    });
    //  }
    this.velocity.y += this.gravity;
    this.x += this.velocity.x;
    this.y += this.velocity.y;
  }

  setBounds(width: number, height: number) {
    this.canvasWidth = width;
    this.canvasHeight = height;
  }
}

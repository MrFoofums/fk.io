import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingComponent } from '../landing/landing.component';
import { ThreeDComponent } from '../three-d/three-d.component';
import { RouterModule, Routes } from '@angular/router';
import { TwodComponent } from '../twod/twod.component';
import { GamesComponent } from '../games/games.component';
import { AboutComponent } from '../about/about/about.component';
import { ResumeComponent } from '../about/resume/resume.component';
import { PrismThreeComponent } from '../physics/prism-three/prism-three.component';
import { CryptoComponent } from '../crypto/crypto.component';
import { RainComponent } from '../rain/rain.component';

const appRoutes: Routes = [
  { path: '3d', component: ThreeDComponent },
  {path: 'home', component: TwodComponent},
  {path: '', component: TwodComponent},
  {path: 'games', component: GamesComponent},
  {path: 'about', component: AboutComponent},
  {path: 'resume', component: ResumeComponent},
  {path: 'prism3d', component: PrismThreeComponent},
  {path: 'crypto', component: CryptoComponent},
  {path: 'dev', component: RainComponent}

];


@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false }
    )
  ],
  exports: [
    RouterModule
  ]
})
export class NavigationModule { }

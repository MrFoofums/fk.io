import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { AnimationComponent } from '../interface/animation-component';
import { AnimationService } from '../service/animation.service';
import { HTML5CanvasObject } from '../interface/html5-canvas-object';
import { UtilsService } from '../service/utils.service';

@Component({
  selector: 'app-orbs',
  templateUrl: './orbs.component.html',
  styleUrls: ['./orbs.component.css']
})
export class OrbsComponent implements  AnimationComponent, AfterViewInit {
  @ViewChild('drawHere') canvas;
  @ViewChild('container') container;

  orbs: Array<Orb>;
  animation: AnimationService;


  constructor(private utils: UtilsService) {
    this.orbs = [];
  }

  init() {
    for (let i = 0; i < 100; i++) {
      this.orbs.push(this.orbFactory());
    }

    this.orbs.forEach(orb => {
      orb.setBounds(this.animation.canvasWidth, this.animation.canvasHeight);
    });
  }

  orbFactory(): Orb {
    const radius = 35;
    const x = this.utils.randomIntFromRange(radius, this.animation.canvasWidth - radius);
    const y = this.utils.randomIntFromRange(radius, this.animation.canvasHeight - radius);
    const vel = {
      x: this.utils.randomIntFromRange(-3, 3),
      y: this.utils.randomIntFromRange(-3, 3)
    };
    return new Orb(x, y, vel, radius, this.utils.randomColor(this.utils.darkColors));
  }

  ngAfterViewInit() {
    this.animation = new AnimationService(this.container, this.canvas);
    this.animation.setUpCanvasAndContext();
    this.init();
    this.animation.animate(this.orbs);
  }

}


class Orb implements HTML5CanvasObject {
  x: number;
  y: number;

  radius: number;
  color: string;

  velocity: {
    x: number;
    y: number;
  };

  canvasWidth: number;
  canvasHeight: number;

  constructor(x: number, y: number, velocity: any, radius: number, color: string) {
    this.x = x;
    this.y = y;
    this.radius = radius;
    this.color = color;
    this.velocity = velocity;
  }


  draw(c: CanvasRenderingContext2D): void {
        c.beginPath();
        c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
        c.fillStyle = this.color;
        c.fill();
        c.closePath();
  }

  update(c: CanvasRenderingContext2D): void {
    this.draw(c);

    if (this.x - this.radius <= 0 || this.x + this.radius >= this.canvasWidth) {
      this.velocity.x = -this.velocity.x;
     }
    if (this.y - this.radius <= 0 || this.y + this.radius >= this.canvasHeight) {
      this.velocity.y = -this.velocity.y;
     }

    this.x += this.velocity.x;
    this.y += this.velocity.y;

  }

  setBounds(width: number, height: number) {
    this.canvasWidth = width;
    this.canvasHeight = height;
  }



}

import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ParticleService } from '../service/particle.service';
import { Geometry, Vector3, PointsMaterial, Points, TextureLoader } from 'three';

@Component({
  selector: 'app-fountain',
  templateUrl: './fountain.component.html',
  styleUrls: ['./fountain.component.css']
})
export class FountainComponent implements AfterViewInit {
  @ViewChild('drawHere') canvas;
  @ViewChild('container') container;

  service: ParticleService;
  mouseX: number;
  mouseY: number;

  // radians: number = 0;
  // maxDistance: number = 3;

  gravity: 0.0001;
  thrust: Vector3;

  constructor() {
  }

  fountainParticles: Array<FountainParticle>;
  
  ngAfterViewInit() {
    this.service = new ParticleService(this.container, this.canvas);
    this.service.setUp();
    this.fountainParticles = [];
    //INIT GEOMETRIES
    this.init();

  }

  init(){
    for(let i =0; i< 1700; i++){
      let x = 0;
      let y = -300;
      let z = 0;

      let fp = new FountainParticle(x,y,z);
      this.fountainParticles.push(fp);
    }
    this.fountainParticles.forEach(p =>{
      this.service.geometry.vertices.push(p);
    });

    this.service.material = new PointsMaterial({size: 15, map: new TextureLoader().load('assets/images/particle.png')});
    this.service.points = new Points(this.service.geometry, this.service.material);
    this.service.addRendereToScene();
    this.animate()
  }

  animate(){
    window.requestAnimationFrame(()=> this.animate());
    this.applyPhysics();
    this.service.points.rotation.y+=.01;
    this.service.camera.lookAt( this.service.scene.position );
    this.service.renderer.render( this.service.scene, this.service.camera );
  }

  applyPhysics(){
    // this.service.points.rotation.y+=.01;
    this.fountainParticles.forEach(p=>{
      p.update();
    })
    this.service.geometry.verticesNeedUpdate = true;
  }

}

class FountainParticle extends Vector3{
  dx: number;
  dy: number;
  dz: number;

  gravity = .04;

  windFactor: number = 2;
  thrust = 8;

  radians: number = 0;

  initalPosition: Vector3;

  constructor(x:number, y:number, z:number){
    super(x,y,z);

    this.dy = this.thrust * Math.random();
    this.dz = (Math.random() -.5) * this.windFactor;
    this.dx = (Math.random() -.5) * this.windFactor;
  }

  update(){
    this.dy-= this.gravity;

    this.x+=this.dx;
    this.y+=this.dy;
    this.z+=this.dz;

    if(this.y < -400){
      this.y = -300;
      this.z= 0;
      this.x = 0;
      this.dy = this.thrust;
    }
  }
}

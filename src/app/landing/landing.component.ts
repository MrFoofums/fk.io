import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { AnimationComponent } from '../interface/animation-component';
import { Particle } from 'src/shared/model/particle';
import { UtilsService } from '../service/utils.service';
import { AnimationService } from '../service/animation.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent {
  @ViewChild('drawHere') canvas;
  @ViewChild('container') container;

  particles: Array<Particle>;
  animation: AnimationService;

  constructor(private gen: UtilsService) {
    this.particles = [];
   }

  init() {
    for (let i = 0; i < 1000; i++) {
      const minRadius = 1;
      const maxRadius = 5;
      const max = this.animation.getMaximumRadiusForCenteredSphere();
      const min = 130;
      // tslint:disable-next-line:max-line-length
      this.particles.push(this.gen.randomParticle(this.animation.canvasWidth / 2, this.animation.canvasHeight / 2, minRadius, maxRadius, min, max));
    }
  }

  ngAfterViewInit() {
    this.animation = new AnimationService( this.container, this.canvas);
    this.animation.setUpCanvasAndContext();
    this.init();
    this.animation.animate(this.particles);
  }

}

import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { AnimationService } from '../service/animation.service';
import { AnimationComponent } from '../interface/animation-component';
import * as THREE from 'three';
import { Scene, PerspectiveCamera, WebGLRenderer } from 'three';
import { ThreeDimensionalObject } from '../interface/three-dimensional-object';

@Component({
  selector: 'app-cubes',
  templateUrl: './cubes.component.html',
  styleUrls: ['./cubes.component.css']
})
export class CubesComponent implements AnimationComponent, AfterViewInit {
  @ViewChild('drawHere') canvas;
  @ViewChild('container') container;

  animation: AnimationService;
  scene: Scene;
  camera: PerspectiveCamera;
  renderer: WebGLRenderer;

  objects: Array<Cube>;

  constructor() {
    this.objects = [];
    const cube = new Cube();
    this.objects.push(cube);
  }

  init() {
  }

  ngAfterViewInit() {
    this.animation = new AnimationService(this.container, this.canvas);
    this.animation.setUp3DCanvasAndContext();

    this.objects.forEach(o => {
      this.animation.scene.add(o.mesh);
    });
    this.animation.camera.position.z = 5;
    this.animation.animate3DObjects(this.objects);

  }

}

class Cube implements ThreeDimensionalObject {
  mesh: THREE.Mesh;

  constructor() {
    const geometry = new THREE.BoxGeometry( 1, 1, 1 );
    const material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
    this.mesh = new THREE.Mesh( geometry, material );
  }

  update(): void {
    this.mesh.rotation.x += .01;
    this.mesh.rotation.y += .01;
  }
}


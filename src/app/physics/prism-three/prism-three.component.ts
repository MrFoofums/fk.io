import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { AnimationComponent } from 'src/app/interface/animation-component';
import { AnimationService } from 'src/app/service/animation.service';
import { Scene, PerspectiveCamera, WebGLRenderer, Color } from 'three';
import * as THREE from 'three';
import { ThreeDimensionalObject } from 'src/app/interface/three-dimensional-object';
import { UtilsService } from 'src/app/service/utils.service';

@Component({
  selector: 'app-prism-three',
  templateUrl: './prism-three.component.html',
  styleUrls: ['./prism-three.component.css']
})
export class PrismThreeComponent implements AnimationComponent, AfterViewInit {
  @ViewChild('drawHere') canvas;
  @ViewChild('container') container;

  animation: AnimationService;
  scene: Scene;
  camera: PerspectiveCamera;
  renderer: WebGLRenderer;

  objects: Array<Prism>;

  constructor(private utils: UtilsService) {
    this.objects = [];
    this.init();
  }

  init() {
    for (let i = 0; i < 95; i ++) {
      this.objects.push(this.randomPrism());
    }
  }

  ngAfterViewInit() {
    this.animation = new AnimationService(this.container, this.canvas);
    this.animation.setUp3DCanvasAndContext();

    this.objects.forEach(o => {
      this.animation.scene.add(o.mesh);
    });
    this.animation.camera.position.z = 60;
    this.animation.animate3DObjects(this.objects);

  }

  randomPrism(): Prism {
    const color = this.utils.randomColor(this.utils.darkColors);
    const maxWidth = 3;
    const maxHeight = 20;
    const maxLength = 3;

    const x = this.utils.randomIntFromRange(-22, 22);
    const z = this.utils.randomIntFromRange(-12, 12);
    const y = 30;

    const width = this.utils.randomIntFromRange(1, maxWidth);
    const height = this.utils.randomIntFromRange(1, maxHeight);
    const length = this.utils.randomIntFromRange(1, maxLength);

    const prism = new Prism(width, height, length, color, x , y, z);
    return prism;
  }

}

class Prism implements ThreeDimensionalObject {
  mesh: THREE.Mesh;

  dx: number;
  dy = .05;
  dz: number;

  rotation: number;

  gravity = .1;

  windFactor = 2;
  thrust = 8;

  constructor(width: number, height: number, length: number, color: any, x: number, y: number, z: number) {
    const geometry = new THREE.BoxGeometry( width, height, length );
    const material = new THREE.MeshBasicMaterial( { color: new Color(color) } );
    this.mesh = new THREE.Mesh( geometry, material );

    this.mesh.position.x = x;
    this.mesh.position.y = y;
    this.mesh.position.z = z;

    this.setDy();
    this.rotation = this.randomIntFromRange(1, 10) / 100;
  }

  setDy() {
    this.dy = this.randomIntFromRange(1, 8) * this.gravity;
  }

  update(): void {
    // this.mesh.rotation.x += .01;
    this.mesh.rotation.y += this.rotation;

    this.mesh.position.y -= this.dy;

    if (this.mesh.position.y < -20) {
      this.resetPrism();
    }
  }

  resetPrism() {
    this.mesh.position.y = 38;
    this.setDy();
    this.mesh.position.x = this.randomIntFromRange(-22, 22);
    this.mesh.position.z = this.randomIntFromRange(-12, 12);

  }

  randomIntFromRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

}

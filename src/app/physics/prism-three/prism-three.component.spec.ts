import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrismThreeComponent } from './prism-three.component';

describe('PrismThreeComponent', () => {
  let component: PrismThreeComponent;
  let fixture: ComponentFixture<PrismThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrismThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrismThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

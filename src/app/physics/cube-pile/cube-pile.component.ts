import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { AnimationComponent } from 'src/app/interface/animation-component';
import * as THREE from 'three';
import { AnimationService } from 'src/app/service/animation.service';
import { Scene, PerspectiveCamera, WebGLRenderer } from 'three';
import { UtilsService } from 'src/app/service/utils.service';

@Component({
  selector: 'app-cube-pile',
  templateUrl: './cube-pile.component.html',
  styleUrls: ['./cube-pile.component.css']
})
export class CubePileComponent implements AnimationComponent, AfterViewInit {
  @ViewChild('drawHere') canvas;
  @ViewChild('container') container;

  animation: AnimationService;
  scene: Scene;
  camera: PerspectiveCamera;
  renderer: WebGLRenderer;

  objects: Array<any>;

  constructor(private utils: UtilsService) {
    this.objects = [];
    this.init();
  }

  ngAfterViewInit() {
  }

  init() {

  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CubePileComponent } from './cube-pile.component';

describe('CubePileComponent', () => {
  let component: CubePileComponent;
  let fixture: ComponentFixture<CubePileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CubePileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CubePileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

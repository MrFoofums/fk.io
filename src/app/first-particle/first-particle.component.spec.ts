import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstParticleComponent } from './first-particle.component';

describe('FirstParticleComponent', () => {
  let component: FirstParticleComponent;
  let fixture: ComponentFixture<FirstParticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstParticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstParticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { AnimationComponent } from '../interface/animation-component';
// tslint:disable-next-line:max-line-length
import { Scene, PerspectiveCamera, WebGLRenderer, ParticleSystem, Geometry, ParticleBasicMaterial, Vector3, AdditiveBlending, TextureLoader, Points, PointsMaterial, BufferGeometry, Texture, Float32BufferAttribute, FogExp2 } from 'three';
import { ParticleService } from '../service/particle.service';

@Component({
  selector: 'app-first-particle',
  templateUrl: './first-particle.component.html',
  styleUrls: ['./first-particle.component.css']
})
export class FirstParticleComponent implements AnimationComponent, AfterViewInit {
  @ViewChild('drawHere') canvas;
  @ViewChild('container') container;

  scene: Scene;
  camera: PerspectiveCamera;

  canvasWidth: number;
  canvasHeight: number;

  renderer: WebGLRenderer;

  pointsField: Points;
  material: PointsMaterial;
  geometry: Geometry;

  sprite: Texture;
  constructor() {
      // We have to wait until AfterViewInit to get canvas width 7 Height
   }

  ngAfterViewInit() {
    this.init();
    this.camera = new PerspectiveCamera(55, this.canvasWidth / this.canvasHeight , 2, 2000);
    this.canvas.setAttribute('width', 0);
    this.canvas.setAttribute('height', 0);
    this.camera.position.z = 1000;

    this.scene = new Scene();

    this.geometry = new Geometry();

    for (let i = 0; i < 2000; i ++) {
      const x = (Math.random() - .5) * 500;
      const y = (Math.random() - .5) * 500;
      const z = (Math.random() - .5) * 500;

      // vector3 seems to work better than not using it
      this.geometry.vertices.push( new Vector3(x, y, z) );
    }

    for (let i = 0; i < 5000; i ++) {
      const x = (Math.random() - .5) * 150;
      const y = (Math.random() - .5) * 150;
      const z = (Math.random() - .5) * 150;

      // vector3 seems to work better than not using it
      this.geometry.vertices.push( new Vector3(x, y, z) );
    }

    // tslint:disable-next-line:max-line-length
    this.material = new PointsMaterial( { color: 0x888888, size: 15, map: new TextureLoader().load('assets/images/particle.png'), transparent: true } );

    this.pointsField = new Points(this.geometry, this.material);
    this.renderer = new WebGLRenderer();
    this.renderer.setSize( window.innerWidth, window.innerHeight );
    this.container.appendChild(this.renderer.domElement);

    this.scene.add(this.pointsField);
    // Animate the thing
    this.animate();
  }


  init() {
    this.container = this.container.nativeElement;
    this.canvas = this.canvas.nativeElement;

    this.canvasHeight = this.container.offsetHeight;
    this.canvasWidth = this.container.offsetWidth;

    this.canvas.setAttribute('width', this.canvasWidth);
    this.canvas.setAttribute('height', this.canvasHeight);

  }

  animate() {
    window.requestAnimationFrame(() => this.animate());

    this.pointsField.rotation.y += .01;
    // this.pointsField.rotation.z+=.01;
    // this.pointsField.rotation.x+=.01;

    this.camera.lookAt( this.scene.position );
    this.renderer.render( this.scene, this.camera );
  }


}


import { Mesh } from 'three';

export interface ThreeDimensionalObject {
    mesh: Mesh;
    update(): void;
}

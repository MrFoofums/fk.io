import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LandingComponent } from './landing/landing.component';
import { PrismsComponent } from './prisms/prisms.component';
import { OrbsComponent } from './orbs/orbs.component';
import { NavigationComponent } from './navigation/navigation.component';
import { CubesComponent } from './cubes/cubes.component';
import { FirstParticleComponent } from './first-particle/first-particle.component';
import { FountainComponent } from './fountain/fountain.component';
import { ThreeDComponent } from './three-d/three-d.component';
import { GamesComponent } from './games/games.component';
import { NavigationModule } from './navigation/navigation.module';
import { TwodComponent } from './twod/twod.component';
import { PrismThreeComponent } from './physics/prism-three/prism-three.component';
import { AboutComponent } from './about/about/about.component';
import { ResumeComponent } from './about/resume/resume.component';
import { AnimationDetailComponent } from './animation-detail/animation-detail.component';
import { CryptoComponent } from './crypto/crypto.component';
import { HttpClientModule } from '@angular/common/http';
import { CubePileComponent } from './physics/cube-pile/cube-pile.component';
import { RainComponent } from './rain/rain.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    PrismsComponent,
    OrbsComponent,
    NavigationComponent,
    CubesComponent,
    FirstParticleComponent,
    FountainComponent,
    ThreeDComponent,
    GamesComponent,
    TwodComponent,
    PrismThreeComponent,
    AboutComponent,
    ResumeComponent,
    AnimationDetailComponent,
    CryptoComponent,
    CubePileComponent,
    RainComponent,
  ],
  imports: [
    BrowserModule,
    NavigationModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

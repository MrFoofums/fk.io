import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { initDomAdapter } from '@angular/platform-browser/src/browser';

@Component({
  selector: 'app-crypto',
  templateUrl: './crypto.component.html',
  styleUrls: ['./crypto.component.scss']
})
export class CryptoComponent implements OnInit {

  coins: Array<Coin>;
  totalValue = 0;

  url = 'http://forrestknight.io:4567/api/v1/coin/';
  value: any;
  constructor(private client: HttpClient) {
    this.init();
    this.setTotalValue();
   }

  ngOnInit() {

  }

  init() {
    this.coins = [];
    const ripple = new Coin('ripple', 10900);
    const stellar = new Coin('stellar', 6500);

    this.coins.push(ripple);
    this.coins.push(stellar);

    this.coins.forEach(coin => {
      coin.getData(this.client, this.url);
    });
  }

  setTotalValue() {
    this.coins.forEach(coin => {
      this.totalValue += coin.value;
    });
  }

}

class Coin {
  id: string;
  amountHeld: number;
  price_usd: any;
  value: number;

  constructor(id: string, amountHeld: number) {
    this.id = id;
    this.amountHeld = amountHeld;
  }

  getData(client: HttpClient, baseUrl: string) {
    client.get(baseUrl + this.id + '/price').subscribe(priceData => {
      this.price_usd = priceData;
      this.value = this.amountHeld * this.price_usd;
    });
  }
}
